(defun make-cd (title artist rating ripped)
  (list :title title :artist artist :rating rating :ripped ripped))

(defvar *db* nil)

(defun add-record (cd)
  (push cd *db*))

(defun dump-db ()
  (dolist (cd *db*)
    (format t "~{~a:~10t~a~%~}~%" cd)))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-for-cd ()
  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")
   (or (parse-integer (prompt-read "Rating") :junk-allowed t) 0)
   (y-or-n-p "Ripped [y/n]: ")))

(defun add-cds ()
  (loop (add-record (prompt-for-cd))
        (if (not (y-or-n-p "Another? [y/n]: ")) (return))))

(defun save-db (filename)
  (with-open-file (out filename
                       :direction :output
                       :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))

(defun load-db (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))

;; The below function can be replaced by the more generic
;; method in the following functions.
;;
;; (defun select-by-artist (artist)
;;   (remove-if-not
;;    #'(lambda (cd) (equal (getf cd :artist) artist))
;;    *db*))

(defun select (selector-fn)
  (remove-if-not selector-fn *db*))

;; The bottom two specific selector functions can be replaced by something
;; more generic, and such is the code that follows this commented-out code.
;; Specifically, a 'where' function, which is commented out, below, having
;; been redefined as a macro further down.

;; (defun artist-selector (artist)
;;   #'(lambda (cd) (equal (getf cd :artist) artist)))

;; (defun title-selector (title)
;;   #'(lambda (cd) (equal (getf cd :title) title)))

;; The below 'where' func is redefined further down as a 'where' macro.
;;
;; (defun where (&key title artist rating (ripped nil ripped-p))
;;   #'(lambda (cd)
;;       (and
;;        (if title    (equal (getf cd :title) title)   t)
;;        (if artist   (equal (getf cd :artist) artist) t)
;;        (if rating   (equal (getf cd :rating) rating) t)
;;        (if ripped-p (equal (getf cd :ripped) ripped) t))))

;; The below 'update' func is redefined further down as a macro.
;; This was not part of the book, but a personal exercise to redefine
;; this function more generically as a macro. I did so below in the
;; 'update' macro
;;
;; (defun update (selector-fn &key title artist rating (ripped nil ripped-p))
;;   (setf *db*
;;         (mapcar
;;          #'(lambda (row)
;;              (when (funcall selector-fn row)
;;                (if title    (setf (getf row :title) title))
;;                (if artist   (setf (getf row :artist) artist))
;;                (if rating   (setf (getf row :rating) rating))
;;                (if ripped-p (setf (getf row :ripped) ripped)))
;;              row) *db*)))

(defun delete-rows (selector-fn)
  (setf *db* (remove-if selector-fn *db*)))

(defmacro backwards (expr) (reverse expr))

(defun make-operations-list (expr-maker fields)
  (loop while fields
        collecting (funcall expr-maker (pop fields) (pop fields))))

(defun make-comparison-expr (field value)
  `(equal (getf cd ,field) ,value))

;; Replaced by 'make-operations-list', above
;; (defun make-comparisons-list (fields)
;;   (loop while fields
;;         collecting (make-comparison-expr (pop fields)  (pop fields))))

(defmacro where (&rest clauses)
  `#'(lambda (cd) (and ,@(make-operations-list #'make-comparison-expr clauses))))

(defun make-update-expr (field value)
  `(setf (getf row ,field) ,value))

;; Replaced by 'make-operations-list', above
;; (defun make-updates-list (fields)
;;   (loop while fields
;;         collecting (make-update-expr (pop fields) (pop fields))))

(defmacro update (selector-fn &rest clauses)
  `(setf *db*
        (mapcar
         #'(lambda (row)
              (when (funcall ,selector-fn row)
                ,@(make-operations-list #'make-update-expr clauses))
              row) *db*)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; With this code, a very simple, but reasonable, database is implemented in
;; 52 lines of code (excluding blank lines and the comments).
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Example usage
(load-db "./CDs.db")
(delete-rows (where :rating 11))
(add-record '(:title "Death Album" :artist "Dethklok" :rating 11 :ripped t))
(update (where :rating 10) :rating 11)
(select (where :ripped nil :rating 11))
(dump-db)
(save-db "./CDs.db")
